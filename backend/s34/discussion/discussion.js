// [SECTION]
// Inserting one document
db.users.insertOne([{
	"firstName": "Jane",
	"lastName": "Doe",
	"contact": {
		"phone": "09123456789",
		"email": "janedoe@gmail.com"
	},
	"courses": ["CSS", "Javascript", "Python"],

}])

// Inserting multiple documents at once
db.users.insertMany([
	{
		"firstName": "John",
		"lastName": "Doe"
	},
	{
		"firstName": "Joseph",
		"lastName": "Doe"
	}
]);


// [SECTION] Retrieving documents
// Retrieving all the inserted users
db.users.find();

// Retrieving a specific document from a collection
db.users.find({ "firstName": "John" });


// [SECTION] Updating existing documents
// For updating a single document
db.users.updateOne(
	{
		"_id": ObjectId("64c1c546304770251cbe3aa0")
	},
	{
		$set: {
			"lastName": "Gaza"
		}
	}
);

// For updating multiple documents
db.users.updateMany(
	{
		"lastName": "Doe"
	},
	{
		$set: {
			"firstName": "Mary"
		}
	}
);


// [SECTION] Deleting documents from a collection
// Deleting multiple documents
db.users.deleteMany({ "lastName": "Doe" });

// Deleting a single document
db.users.deleteOne({
	"_id": ObjectId("64c1c546304770251cbe3aa0")
});