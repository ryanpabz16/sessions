const User = require("../models/User.js");

module.exports.createNewUser = (request_body) => {
    return User.findOne({ username: request_body.username })
        .then((result, error) => {
            if(result !== null && result.username === request_body.username) {
                return "Duplicate username found";
            }
            
            if(!request_body.username || !request_body.password) {
                return "Both username and password must be provided";
            }

            let newUser = new User({
                username: request_body.username,
                password: request_body.password
            });

            return newUser.save().then((savedUser, error) => {
                if(error) {
                    return { message: error.message };
                }
                
                return "New user registered: " + savedUser;
            });
        });
};