const Task = require("../models/Task.js");

module.exports.getTask = (request_url) => {
    let [_, urlId] = request_url.split("/");

    return Task.findById(urlId).then((result, error) => {
        if(error) {
            return { message: error.message };
        }

        return { task: result };
    });
};

module.exports.createNewTask = (request_url) => {
    let [_, urlId, urlStatus] = request_url.split("/");

    return Task.findByIdAndUpdate(urlId, { status: urlStatus}).then((result, error) => {
        if(error) {
            return { message: error.message };
        }

        return result;
    });
};