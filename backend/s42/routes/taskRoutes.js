const express = require("express");
const router = express.Router();
const TaskController = require("../controllers/TaskController.js");

router.get("/:id", (request, response) => {
    TaskController.getTask(request.url).then(result => {
        response.send(result);
    });
});

router.put("/:id/:status", (request, response) => {
    TaskController.createNewTask(request.url).then(result => {
        response.send(result);
    });
});


module.exports = router;