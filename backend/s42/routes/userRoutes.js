const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController.js");

router.post("/", (request, response) => {
    UserController.createNewUser(request.body).then(result => {
        response.send(result);
    });
});


module.exports = router;