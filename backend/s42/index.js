require('dotenv').config();
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js");
const userRoutes = require("./routes/userRoutes.js");
const app = express();
const port = 4000;


mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-pabualan.kz0zb91.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let database = mongoose.connection;

database.on("error", () => console.log("Connection error :("));
database.once("open", () => console.log("Connected to MongoDB!"));


app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/tasks", taskRoutes);
app.use("/api/signup", userRoutes);

app.listen(port, () => console.log(`Server is running at localhost:${port}`));


module.exports = app;