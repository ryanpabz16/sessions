// ARGUMENTS AND PARAMETERS
function printName(name) {
	console.log("I'm the real " + name);
}

printName("aespa");


function checkDivisibilityBy2(number) {
	let result = number % 2;

	console.log("The remainder of " + number + " is " + result);
}

checkDivisibilityBy2(15);


// MULTIPLE ARGUMENTS AND PARAMETERS
function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Ryan", "Pabualan");


// USAGE OF PROMPTS AND ALERTS
let user_name = prompt("Enter your username: ");

function displayWelcomeMessageForUser(userName) {
	alert("Welcome back to World, " + userName);
}

displayWelcomeMessageForUser(user_name);