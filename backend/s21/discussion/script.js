// FUNCTION DECLARATION AND INVOCATION
function printName() {
	console.log("My name is Ryan");
}

printName();


// FUNCTION EXPRESSION
let variable_function = function() {
	console.log("Hello from function expression!");
}

variable_function();


// SCOPING
let global_variable = "Call me Mr. Worldwide";

console.log(global_variable);

function showNames() {
	let function_variable = "Ryan";
	const function_const = "Angelo";

	console.log(function_variable);
	console.log(function_const);

	// You CAN use global variables inside any function as long as they are declared outside of the function scope.
	console.log(global_variable);
}

// You CANNOT use locally-scoped variables outside the function they are declare in.
// console.log(function_variable);

showNames();


// NESTED FUNCTIONS
function parentFunction() {
	let name = "Sam";

	function childFunction() {
		let nested_name = "Frank";

		console.log(name);

		// Accessing the "nested_name" variable within the same function it was declared in WILL WORK.
		console.log(nested_name);
	}

	childFunction();

	// Accessing the "nesting_name" variable OUTSIDE the function it was delcared in, WILL NOT work.
	// console.log(nested_name);
}

parentFunction();


// BEST PRACTICE FOR FUNCTION NAMING
function printWelcomeMessageForUser() {
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, " + first_name + " " + last_name + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessageForUser();


// RETURN STATEMENT
function fullName() {
	return "Angelo Ryan Pabualan"
}

console.log(fullName());