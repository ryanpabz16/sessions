// [SECTION] Server variables for initialization
const express = require("express"); // Imports express
const app = express(); // Initializes express
const port = 4000;

// [SECTION] Middleware
app.use(express.json()); // Registering a middleware that will make express be able to read JSON format from requests
app.use(express.urlencoded({ extended: true })); // A middleware that will allow express to be able to read data types other than the default string and array it can usually read.

// [SECTION] Server listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`));

// [SECTION] Routes
app.get("/", (req, res) => {
    res.send("Hello World!");
});

app.post("/greeting", (req, res) => {
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
});

let users = []
app.post("/register", (req, res) => {
    if(req.body.username && req.body.password) {
        users.push(req.body);

        res.send(`User ${req.body.username} has successfully been registered!`);
    } else {
        res.send("Please input BOTH username and password.");
    }
})

module.exports = app;