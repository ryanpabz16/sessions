const Course = require("../models/Course.js");


module.exports.addCourse = (request_body) => {
    let new_course = new Course({
        name: request_body.name,
        description: request_body.description,
        price: request_body.price
    });

    return new_course.save().then((course, error) => {
        if (error) return { message: error.message };

        return {
            message: "New course has been added successfully!",
            data: course
        };
    }).catch(error => console.error(error));
}

module.exports.getAllCourses = () => {
    return Course.find().then(result => {
        return result;
    }).catch(error => console.error(error));
}

module.exports.getAllActiveCourses = () => {
    return Course.find({ isActive: true }).then(result => {
        return result;
    }).catch(error => console.error(error));
}

module.exports.getCourse = (request_id) => {
    return Course.findById(request_id).then(result => {
        return result;
    }).catch(error => console.error(error));
}

module.exports.updateCourse = (request_id, request_body) => {
    return Course.findByIdAndUpdate(
        request_id,
        {
            name: request_body.name,
            description: request_body.description,
            price: request_body.price
        },
        { new: true }
    ).then((result, error) => {
        if (error) return { message: error.message };

        return {
            message: "Course has been updated successfully!",
            data: result
        };
    }).catch(error => console.error(error));
}

module.exports.archiveCourse = (request_id) => {
    return Course.findByIdAndUpdate(
        request_id,
        { isActive: false },
        { new: true }
    ).then((result, error) => {
        if (error) return { message: error.message };

        return {
            message: "Course has been archived!",
            data: result
        };
    }).catch(error => console.error(error));
}

module.exports.activateCourse = (request_id) => {
    return Course.findByIdAndUpdate(
        request_id,
        { isActive: true },
        { new: true }
    ).then((result, error) => {
        if (error) return { message: error.message };

        return {
            message: "Course has been activated!",
            data: result
        };
    }).catch(error => console.error(error));
}