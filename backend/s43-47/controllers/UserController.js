const bcrypt = require("bcrypt");
const User = require("../models/User.js");
const Course = require("../models/Course.js");
const auth = require("../auth.js");


module.exports.checkEmailExists = (request_email) => {
    return User.find({ email: request_email }).then((users, error) => {
        if (error) return { message: error.message };

        return users.length > 0;
    });
}

module.exports.registerUser = (request_body) => {
    let new_user = new User({
        firstName: request_body.firstName,
        lastName: request_body.lastName,
        email: request_body.email,
        mobileNo: request_body.mobileNo,
        password: bcrypt.hashSync(request_body.password, 10)
    });

    return new_user.save().then((user, error) => {
        if (error) return { message: error.message };

        return {
            message: "User successfully registered!",
            data: user
        };
    }).catch(error => console.log(error));
}

module.exports.loginUser = (request_body) => {
    return User.findOne({ email: request_body.email }).then(user => {
        if (!user) return { message: "The user isn't registered yet"};

        const isPasswordCorrect = bcrypt.compareSync(request_body.password, user.password);
        if (isPasswordCorrect) return { accessToken: auth.createAccessToken(user) };

        return { message: "Your password is incorrect"};
    }).catch(error => console.log(error));
}

module.exports.getProfile = (request_id) => {
    return User.findById(request_id).then(user => {
        if (!user) return { message: "The user isn't registered yet"};   
        
        user.password = ""; 
        return user;
    }).catch(error => console.log(error));
}

// module.exports.enroll = async (request_user, request_body) => {
//     if (request_user.isAdmin) return { message: "Action forbidden!" };

//     let isUserUpdated = await User.findById(request_user.id)
//         .then(user => {
//             let new_enrollment = { courseId: request_body.courseId };

//             user.enrollments.push(new_enrollment);

//             return user.save().then(updated_user => true)
//                 .catch(error => console.error(error));
//         });

//     if (!isUserUpdated) return { message: isUserUpdated };

//     let isCourseUpdated = await Course.findById(request_body.courseId)
//         .then(course => {
//             let new_enrollee = { userId: request_user.id };

//             course.enrollees.push(new_enrollee);
//             return course.save().then(updated_course => true)
//                 .catch(error => console.error(error));
//         });

//     if (!isCourseUpdated) return { message: isCourseUpdated };

//     return {
//         message: "Course enrolled successfully!"
//     };
// }

module.exports.enroll = async (request_user, request_body) => {
    if (request_user.isAdmin) return { message: "Action forbidden!" };

    return await User.findById(request_user.id).then(user => {
        return Course.findById(request_body.courseId).then(course => {
            if (user && course) {
                user.enrollments.push({ courseId: course._id });
                course.enrollees.push({ userId: user._id });

                return user.save().then((updated_user, error) => {
                    if (error) return { message: error.message };

                    return course.save().then((updated_course, error) => {
                        if (error) return { message: error.message };

                        return {
                            message: "Course enrolled successfully!",
                            data: [updated_user, updated_course]
                        };
                    });
                });
            };
        });
    });
}