const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();
const port = 4000;
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use("/api/users", userRoutes);
app.use("/api/courses", courseRoutes);

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-pabualan.kz0zb91.mongodb.net/booking-system-api?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

mongoose.connection.on("error", () => console.log("Cannot connect to database!"));
mongoose.connection.once("open", () => console.log("Connected to MongoDB!"));

app.listen(process.env.PORT || port, () => {
    console.log(`Booking System API is running at localhost:${process.env.PORT || port}`);
});