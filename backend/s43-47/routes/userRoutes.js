const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController.js");
const { verify, verifyAdmin } = require("../auth.js");


router.post("/check-email", (request, response) => {
    UserController.checkEmailExists(request.body.email).then(result => {
        response.send(result);
    });
});

router.post("/register", (request, response) => {
    UserController.registerUser(request.body).then(result => {
        response.send(result);
    });
});

router.post("/login", (request, response) => {
    UserController.loginUser(request.body).then(result => {
        response.send(result);
    });
});

router.post("/details", verify, verifyAdmin, (request, response) => {
    UserController.getProfile(request.body.id).then(result => {
        response.send(result);
    });
});

router.post("/enroll", verify, (request, response) => {
    UserController.enroll(request.user, request.body).then(result => {
        response.send(result);
    });
});


module.exports = router;