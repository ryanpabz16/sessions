const router = require("express").Router();
const CourseController = require("../controllers/CourseController.js");
const { verify, verifyAdmin } = require('../auth');

router.post("/", verify, verifyAdmin, (request, response) => {
    CourseController.addCourse(request.body).then(result => {
        response.send(result);
    });
});

router.get("/all", (request, response) => {
    CourseController.getAllCourses().then(result => {
        response.send(result);
    });
});

router.get("/", (request, response) => {
    CourseController.getAllActiveCourses().then(result => {
        response.send(result);
    });
});

router.get("/:id", (request, response) => {
    CourseController.getCourse(request.params.id).then(result => {
        response.send(result);
    });
});

router.put("/:id", verify, verifyAdmin, (request, response) => {
    CourseController.updateCourse(request.params.id, request.body).then(result => {
        response.send(result);
    });
});

router.put("/:id/archive", verify, verifyAdmin, (request, response) => {
    CourseController.archiveCourse(request.params.id).then(result => {
        response.send(result);
    });
});

router.put("/:id/activate", verify, verifyAdmin, (request, response) => {
    CourseController.activateCourse(request.params.id).then(result => {
        response.send(result);
    });
});

module.exports = router;