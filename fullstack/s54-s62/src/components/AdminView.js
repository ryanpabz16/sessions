import { useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";

export default function AdminView({ coursesData }) {
    const [ courses, setCourses ] = useState([]);

    useEffect(() => {
        const courses_array = coursesData.map(course => {
            return <tr key={course._id}>
                {/* <td>{course._id}</td> */}
                <td>{course.name}</td>
                <td>{course.description}</td>
                <td>{course.price}</td>
                <td>{course.isActive ? "Available" : "Unavailable"}</td>
                <td>
                    <Button variant="primary">Edit</Button>
                    <hr />
                    <Button variant="warning">Archive</Button>
                </td>
            </tr>;
        });

        setCourses(courses_array);
    });
    
    return <>
        <h1 className="mt-5">Admin Dashboard</h1>

        <Table striped bordered hover responsive>
            <thead>
                <tr>
                    {/* <th>ID</th> */}
                    <th>Name</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Availability</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {courses}
            </tbody>
        </Table>
    </>;
}