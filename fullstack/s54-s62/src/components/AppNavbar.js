import { useContext } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { Link, NavLink } from "react-router-dom";
import UserContext from "../UserContext";

export default function AppNavbar() {
    const { user } = useContext(UserContext);
    return (
        <Navbar bg="dark" variant="dark" expand="md">
            <Container fluid>
                <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
                
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
                        {
                            (user.isAdmin)
                                ? <Nav.Link as={NavLink} to="/courses/addCourse">Add Course</Nav.Link> : ""
                        }
                        {
                            (!user.id)
                                ? <>
                                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                </>
                                : <>
                                    <Nav.Link as={NavLink} to="/profile">Profile</Nav.Link>
                                    <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                                </>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}