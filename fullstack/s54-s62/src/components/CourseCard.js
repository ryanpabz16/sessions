import { Card } from "react-bootstrap";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function CourseCard(props) {
    const { _id: id, name, description, price } = props.course;

    return (
        <Card id="courseComponent">
            <Card.Body>
                <Card.Title>{name}</Card.Title>

                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>Php {price}</Card.Text>
                
                <Link className="btn btn-primary" to={`/courses/${id}`}>View Details</Link>
            </Card.Body>
        </Card>
    );
}

CourseCard.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
    })
};