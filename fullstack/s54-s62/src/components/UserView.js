import { useEffect, useState } from "react";
import CourseCard from "./CourseCard";

export default function UserView({ coursesData }) {
    const [ courses, setCourses ] = useState([]);

    useEffect(() => {
        const active_courses = coursesData
            .filter(course => course.isActive)
            .map(course => {
                return <CourseCard course={course} key={course._id} />;
        });
        
        setCourses(active_courses);
    }, [coursesData]);

    return <>
        <h1 className="my-5 text-center">Courses</h1>
        { courses }
    </>;
}