import { Form, Button } from "react-bootstrap";
import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Login() {
    const { user, setUser } = useContext(UserContext);
    
    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    const [ isActive, setIsActive ] = useState(false);

    useEffect(() => {
        if (email && password) setIsActive(true);
        else setIsActive(false);
    }, [email, password]);

    const authenticate = (event) => {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(response => response.json()).then(result => {
            if (result.accessToken) {
                localStorage.setItem("token", result.accessToken);

                Swal.fire({
                    icon: "success",
                    title: "Thank you for logging in"
                });

                retrieveUserDetails(result.accessToken, result.userId);
                
                setEmail("");
                setPassword("");
            } else {
                Swal.fire({
                    icon: "error",
                    title: result.message
                });
            }
        });
    };

    const retrieveUserDetails = (token, userId) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ id: userId })
        }).then(response => response.json()).then(result => {
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            });
        });
    };

    return (
        (user.id)
            ? <Navigate to="/courses" />
            : <Form onSubmit={ event => authenticate(event) }>
                <h1 className="my-5 text-center">Login</h1>
                <Form.Group>
                    <Form.Label>Email Address:</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Email" 
                        required 
                        value={email}
                        onChange={event => setEmail(event.target.value)}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Password:</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        required 
                        value={password}
                        onChange={event => setPassword(event.target.value)}
                    />
                </Form.Group>
                <Button type="submit" disabled={!isActive}>Login</Button>

            </Form>
    );
}