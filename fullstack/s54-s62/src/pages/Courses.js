import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext.js";
import AdminView from "../components/AdminView.js";
import UserView from "../components/UserView.js";

export default function Courses() {
    const { user } = useContext(UserContext);
    const [ courses, setCourses ] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
            .then(response => response.json()).then(result => {
                setCourses(result);
            });
    }, []);

    return <>
            {
                (user.isAdmin)
                ? <AdminView coursesData={courses} />
                : <UserView coursesData={courses} />
            }
    </>;
}