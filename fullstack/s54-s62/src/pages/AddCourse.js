import { useContext, useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import { Button, Form } from "react-bootstrap";
import Swal from "sweetalert2";

export default function AddCourse() {
    const navigate = useNavigate();
    const { user } = useContext(UserContext);

    const [ name, setName ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ price, setPrice ] = useState(0);

    function createCourse(event) {
        event.preventDefault();
        const token = localStorage.getItem("token");

        fetch(`${process.env.REACT_APP_API_URL}/courses`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${token}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        }).then(response => response.json()).then(result => {
            console.log(result);

            if ("data" in result) {
                Swal.fire({
                    icon: "success",
                    title: result.message
                });

                setName("");
                setDescription("");
                setPrice(0);

                navigate("/courses");
            } else {
                Swal.fire({
                    icon: "error",
                    title: result.message
                });
            }
        });
    }

    return (
        (user.isAdmin)
            ? <Form onSubmit={createCourse}>
                <h1 className="my-5 text-center">Add Course</h1>
                <Form.Group>
                    <Form.Label>Name:</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Name" 
                        required 
                        value={name} 
                        onChange={event => setName(event.target.value)}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Enter Description" 
                        required 
                        value={description} 
                        onChange={event => setDescription(event.target.value)}
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Price:</Form.Label>
                    <Form.Control 
                        type="number" 
                        placeholder="Enter Price" 
                        required 
                        value={price} 
                        onChange={event => setPrice(event.target.value)}
                    />
                </Form.Group>

                <Button type="submit" className="my-5">Submit</Button>
            </Form>
            : <Navigate to="/courses" />
    );
}