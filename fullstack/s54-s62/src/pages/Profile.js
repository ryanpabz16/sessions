import { useContext, useEffect, useState } from "react";
import UserContext from "../UserContext";
import { Col, Row } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Profile() {
    const { user } = useContext(UserContext);

    const [ details, setDetails ] = useState({});

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ id: user.id })
        }).then(response => response.json()).then(result => {
            console.log(result);
            if ("_id" in result) {
                setDetails(result);
            } else {
                Swal.fire({
                    icon: "error",
                    title: result.message
                });
            }
        });
    }, [user.id]);

    return (
        (!user.id)
            ? <Navigate to="/login" />
            : <>
                <Row className="p-5 text-center">
                    <h1>Profile</h1>
                </Row>
                <Row>
                    <Col md>
                        <h1>{details.firstName} {details.lastName}</h1>
                    </Col>
                    <Col md>
                        <ul>
                            <li>Email: {details.email}</li>
                            <li>Mobile Number: {details.mobileNo}</li>
                        </ul>
                    </Col>
                </Row>
            </>
    );
}