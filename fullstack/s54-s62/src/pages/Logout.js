import { useContext, useEffect } from "react";
import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);
    unsetUser();

    useEffect(() => {
        setUser({
            id: null,
            isAdmin: null
        });

        localStorage.clear();

        Swal.fire({
            icon: "success",
            title: "Goodbye"
        });
    }, [setUser]);

    return (
        <Navigate to="/login" />
    );
}