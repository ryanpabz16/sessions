import { useContext, useEffect, useState } from "react";
import { Button, Card, Col, Container, Row } from "react-bootstrap";
import { Link, useNavigate, useParams } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function CourseItem() {
    const { user } = useContext(UserContext);
    const { courseId } = useParams();
    const navigate = useNavigate();

    const [ name, setName ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ price, setPrice ] = useState(0);

    const enroll = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
            method: "POST",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                courseId: courseId
            })
        }).then(response => response.json()).then(result => {
            console.log(result);
            if ("data" in result) {
                Swal.fire({
                    icon: "success",
                    title: result.message
                });

                console.log(result.data);
            } else {
                Swal.fire({
                    icon: "error",
                    title: result.message
                });
            }

            navigate("/courses");
        });
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
            .then(response => response.json()).then(result => {
                setName(result.name);
                setDescription(result.description);
                setPrice(result.price);
            });
    });

    return (
        <Container className="mt-5">
            <Row>
                <Col>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title><h1>{name}</h1></Card.Title>

                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>

                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>

                            <Card.Subtitle>Class Schedule</Card.Subtitle>
                            <Card.Text>7:43AM - 6:06PM</Card.Text>

                            {
                                (user.id)
                                    ? <Button onClick={enroll}>Enroll</Button>
                                    : <Link className="btn btn-danger btn-block"
                                        to="/login">Log In to Enroll</Link>
                            }
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}