import { Row } from "react-bootstrap";

export default function NotFound() {
    return (
        <Row className="p-5 g-1">
            <h1>Not Found</h1>
            <p>The page you visited could not be found</p>
        </Row>
    );
}