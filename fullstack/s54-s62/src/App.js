import "./App.css";
import '@sweetalert2/themes/bootstrap-4/bootstrap-4.css';
import { Container } from "react-bootstrap";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Routes } from "react-router-dom";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import Courses from "./pages/Courses";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Profile from "./pages/Profile";
import NotFound from "./pages/NotFound";
import { UserProvider } from "./UserContext";
import { useEffect, useState } from "react";
import CourseItem from "./pages/CourseItem";
import AddCourse from "./pages/AddCourse";

function App() {
    const [ user, setUser ] = useState(() => {
        if ("user" in localStorage) {
            return JSON.parse(localStorage.user);
        } else {
            return {
                id: null,
                isAdmin: null
            };
        }
    });

    const unsetUser = () => {
        localStorage.clear();
    };

    useEffect(() => {
        localStorage.user = JSON.stringify(user);
        console.log(user);
    }, [user]);

    return (
        <UserProvider value={{ user, setUser, unsetUser }}>
            <Router>
                <AppNavbar />
                <Container>
                    <Routes>
                        <Route path="/" element={ <Home /> }/>
                        <Route path="/courses" element={ <Courses /> }/>
                        <Route path="/courses/addCourse" element={ <AddCourse /> }/>
                        <Route path="/courses/:courseId" element={ <CourseItem /> }/>
                        <Route path="/register" element={ <Register /> }/>
                        <Route path="/login" element={ <Login /> }/>
                        <Route path="/logout" element={ <Logout /> }/>
                        <Route path="/profile" element={ <Profile /> }/>
                        <Route path="/*" element={ <NotFound /> }/>
                    </Routes>
                </Container>
            </Router>
        </UserProvider>
    );
}

export default App;
